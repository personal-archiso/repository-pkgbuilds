#!/bin/bash

sudo pacman -Syu --noconfirm

cd $(dirname $0)
folders=`find ./x86_64/ -name 'PKGBUILD' -exec dirname "{}" \;`
paru -B ${folders} --noconfirm --localrepo $@
# paru -B ${folders} --noconfirm --localrepo --nokeepsrc --rebuild

# Clean paru cache with
# paru -Sc

# Old method

# cd x86_64
# find -name PKGBUILD -execdir sh -c 'makepkg --printsrcinfo > .SRCINFO' \;
# aur graph */.SRCINFO | tsort | tac > queue # Remove unwanted targets
# aur build -a queue -f -s -C --noconfirm
